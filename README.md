# VJSoft

## About
Small VJ software made with openframeworks.

## Main features
  - Photoshop like layers with dynamic reordering
  - "Infinte stack" of filters for layers with dynamic reordering
  - Audio sync (added JACK support in linux)
  - IP/Webcam input  

## Required addons
ofxGuiExtended    
ofxSortableList    
ofxFft    
ofxIpVideoGrabber    
ofxPoco    
ofxXmlSettings

## Troubleshooting
[Compile error with latest openAL and of 0.11 in linux.](https://forum.openframeworks.cc/t/compilation-failing-due-to-confliting-definition-in-openal/33927/2)  
Latest make not compiling. Downgrade to version 12.x.