#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGLFWWindow.h"

//========================================================================
int main( ){

	std::locale::global(std::locale("fi_FI.utf8"));
	ofGLFWWindowSettings settings;

	settings.setGLVersion(3,3);
	settings.setSize(640, 480);
	shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(settings);			// <-------- setup the GL context

	settings.setSize(1280, 720);
	settings.shareContextWith = mainWindow;
	shared_ptr<ofAppBaseWindow> viewerWindow = ofCreateWindow(settings);			// <-------- setup the GL context

	settings.setSize(1280 * 0.5, 720 * 0.5);
	settings.shareContextWith = mainWindow;
	shared_ptr<ofAppBaseWindow> previewWindow = ofCreateWindow(settings);			// <-------- setup the GL context

	shared_ptr<ofApp> mainApp(new ofApp);
	ofAddListener(viewerWindow->events().draw, mainApp.get(), &ofApp::drawOutput);
	ofAddListener(previewWindow->events().draw, mainApp.get(), &ofApp::drawPreview);

	ofRunApp(mainWindow, mainApp);

	ofRunMainLoop();
}
