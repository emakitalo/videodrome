#pragma once

#include "ofMain.h"
#include "ofEffect.h"
#include "IPVideoGrabber.h"

class ofVJObj : public ofVideoPlayer {
	public:
		void init();
		void applyEffect(ofEffect *effect);

		ofParameter<bool> name;
		ofParameter<int> type;
		ofParameter<int> blendMode;
		ofParameter<bool> playing;
		ofParameter<float> playbackSpeed;
		ofParameter<float> opacity;
		ofParameter<ofVec2f> size;
		ofParameter<bool> isCamera;
		ofParameter<bool> isWebCam;
		ofParameter<string> userName;
		ofParameter<string> userPassword;
		ofParameter<string> camIP;
		ofParameter<void> removeEffect;
		vector<ofFbo> textures;
		bool activeTextureId = true;
		ofFbo activeTexture;

		int id;

		ofxGuiGroup *controlPanel;

		vector<ofEffect> effects; 
		ofxGuiPanel *effectsPanel;
		ofxSortableList *effectsList;
		ofxGuiGroup *activeEffectGui;
		int activeEffectId;

		vector<float> audio;
		shared_ptr<ofx::Video::IPVideoGrabber> camera;

		ofVJObj() {
		}
};
