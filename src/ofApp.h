#pragma once

#include "ofMain.h"
#include "ofVJObj.h"
#include "ofxGuiExtended.h"
#include "ofxSortableList.h"
#include "ofxFft.h"
#include "IPVideoGrabber.h"
#include "ofEffect.h"

class ofApp: public ofBaseApp {
	public:
		void setup();
		void update();
		void draw();
		void exit();
		void mouseScrolled(float x, float y);
		void selectLayer(int& index);
		void togglePlay(bool &state);
		void setPlaybackSpeed(float &speed);
		void audioReceived(float* input, int bufferSize, int nChannels);
		void addLayer(); 
		void addEffect(int &id); 
		void handleLayer(ofVJObj *layer);
		void removeLayer();
		void removeEffect();
		void layerMoved(MovingElementData &data);
		void effectMoved(MovingElementData &data);
		void drawOutput(ofEventArgs &args); 
		void drawPreview(ofEventArgs &args); 
		ofFileDialogResult openFile();
		void initAudio();
		void initEffects();
		void initLayers();
		void initWebCam(bool &state);
		void initCamera(bool &state);

		// Basic shit
		float width;
		float height;
		float fbow;
		float fboh;

		// Layers
		vector<ofVJObj*> layers;
		ofVJObj* activeLayer;
		vector<ofBlendMode> modes;

		// WebCam
		ofVideoGrabber webcam;

		// Audio 
		ofSoundStream stream;
		ofxFft* fft;
		ofMutex soundMutex;
		vector<float> drawBins, middleBins, audioBins, smoothBins;
		ofRectangle spectogram;
		int bufferSize;

		// GUI
		//// Base
		ofxGui gui;
		ofxGuiPanel* layerPanelGui;
		ofxSortableList* layersGui;

		ofParameter<void> addFileToggle;
		ofParameter<void> removeFileToggle;

		//// Effects
		ofxSortableList *effects;
		ofxGuiPanel *effectsPanel;
		map<string, ofParameter<bool>> effectParameters;
		vector<string> allEffects;
		string activeEffect;
		ofShader feedbackEffect;
		ofShader mixer;

		ofxSortableList *fbParameters;
		ofxGuiPanel *fbPanel;

		ofParameter<float> feedback;
		ofParameter<float> blurSize;
		ofParameter<int> blurSamples;
		ofParameter<float> brightness;
		ofParameter<float> contrast;
		ofParameter<float> hue;
		ofParameter<float> saturation;
		ofParameter<float> noiseOpacity;
		ofParameter<float> noiseContrast;
		ofParameter<float> noiseSpeed;
		ofParameter<float> noiseScale;
		ofParameter<float> noiseScale2;
		ofParameter<float> noiseDistance;
		ofParameter<float> noiseRotation;
		ofParameter<int> audioChannel;
		ofParameter<float> audioMultiplier;

		// Output
		ofFbo layer_0;
		ofFbo output;

		ofJson containerConf = {
			{"background-color", "#ff0077"}
		};
};
