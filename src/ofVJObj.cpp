#include "ofVJObj.h"

void ofVJObj::applyEffect(ofEffect *effect) {
	textures[activeTextureId].begin();
		effect->begin();
			effect->setUniformTexture("tex0", textures[!activeTextureId].getTexture(), 1);
			for(auto i : effect->floats) {
				effect->setUniform1f(i->getName(), i->get());
			}
			for(auto i : effect->vec2s) {
				effect->setUniform2f(i->getName(), i->get());
			}
			for(auto i : effect->vec3s) {
				effect->setUniform3f(i->getName(), i->get());
			}
			for(auto i : effect->ints) {
				effect->setUniform1i(i->getName(), i->get());
			}
			if(effect->getUniformLocation("time") > -1) {
				effect->setUniform1f("time", ofGetElapsedTimeMillis());
			}
			if(effect->getUniformLocation("audio") > -1) {
				effect->setUniform1fv("audio", &audio[0], audio.size());
			}
			textures[!activeTextureId].draw(0, 0, effect->width, effect->height);
		effect->end();
	textures[activeTextureId].end();
	activeTexture = textures[activeTextureId];
	activeTextureId = !activeTextureId;
}

