#include "ofApp.h"

void ofApp::setup() {
	ofBackground(0);
	ofSetVerticalSync(true);
	ofSetFrameRate(25);

	width = 1280;
	height = 720;
	layer_0.allocate(width, height, GL_RGBA16);
	output.allocate(width, height, GL_RGBA16);

	initAudio();
	initEffects();
	initLayers();
}

void ofApp::initAudio() {
	stream.printDeviceList();

	bufferSize = 32;
	ofSoundStreamSettings settings;

	auto devices = stream.getMatchingDevices("default");
	if(!devices.empty()){
		settings.setInDevice(devices[1]);
	}

	settings.setInListener(this);
	settings.sampleRate = 44100;
	settings.numOutputChannels = 0;
	settings.numInputChannels = 2;
	settings.bufferSize = bufferSize;
	stream.setup(settings);

	fft = ofxFft::create(bufferSize, OF_FFT_WINDOW_HAMMING);
	drawBins.resize(fft->getBinSize());
	middleBins.resize(fft->getBinSize());
	audioBins.resize(fft->getBinSize());
	smoothBins.resize(fft->getBinSize());
}

void ofApp::audioReceived(float* input, int bufferSize, int nChannels) {
	float maxValue = 0;
	for(int i = 0; i < bufferSize; i++) {
		if(abs(input[i]) > maxValue) {
			maxValue = abs(input[i]);
		}
	}

	for(int i = 0; i < bufferSize; i++) {
		input[i] /= maxValue;
	}

	fft->setSignal(input);

	float* curFft = fft->getAmplitude();

	for(int i = 0; i < fft->getBinSize(); i++) {
		if(abs(smoothBins[i]) < abs(curFft[i])) {
			smoothBins[i] = curFft[i];
		} else {
			smoothBins[i] *= 0.997;
		}
			
		audioBins[i] += 0.1 * (smoothBins[i] - audioBins[i]);
	}

	soundMutex.lock();
	middleBins = audioBins;
	soundMutex.unlock();
}

void ofApp::initEffects() {
	effectsPanel = gui.addPanel("Effects", containerConf);
	effects = effectsPanel->add<ofxSortableList>("Layers");
	effects->setExclusiveToggles(true);
	effects->getActiveToggleIndex().addListener(this, &ofApp::addEffect);

	allEffects.push_back("br-co");
	effectParameters[allEffects[0]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[0]].set(allEffects[0], false));

	allEffects.push_back("noise");
	effectParameters[allEffects[1]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[1]].set(allEffects[1], false));

	allEffects.push_back("hsv");
	effectParameters[allEffects[2]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[2]].set(allEffects[2], false));

	allEffects.push_back("dsf");
	effectParameters[allEffects[3]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[3]].set(allEffects[3], false));

	allEffects.push_back("shapes");
	effectParameters[allEffects[4]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[4]].set(allEffects[4], false));

	allEffects.push_back("blur");
	effectParameters[allEffects[5]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[5]].set(allEffects[5], false));

	allEffects.push_back("glitch");
	effectParameters[allEffects[6]] = ofParameter<bool>();
	effects->add(effectParameters[allEffects[6]].set(allEffects[6], false));

	fbPanel = gui.addPanel("FEEDBACK", containerConf);
	fbParameters = fbPanel->add<ofxSortableList>("parameters");

	fbParameters->add(feedback.set("feedback",0.5,0,1));
	fbParameters->add(blurSize.set("blurSize",0,0,50));
	fbParameters->add(blurSamples.set("blurSamples",0,0,10));
	fbParameters->add(brightness.set("brightness",0,-2,2));
	fbParameters->add(contrast.set("contrast",1,-2,2));
	fbParameters->add(hue.set("hue",0,0,5));
	fbParameters->add(saturation.set("saturation",1,0,5));
	fbParameters->add(noiseOpacity.set("noiseOpacity",0.5,0,1));
	fbParameters->add(noiseContrast.set("noiseContrast",1,0,2));
	fbParameters->add(noiseSpeed.set("noiseSpeed",0.01,0,1));
	fbParameters->add(noiseScale.set("noiseScale",0.001,0,0.1));
	fbParameters->add(noiseScale2.set("noiseScale2",0.5,0,1));
	fbParameters->add(noiseDistance.set("noiseDistance",0.1,0,20));
	fbParameters->add(noiseRotation.set("noiseRotation",0.1,0,4));
	fbParameters->add(audioChannel.set("audioChannel",0,0,bufferSize/2-1));
	fbParameters->add(audioMultiplier.set("audioMultiplier",0,0,20));

	modes.push_back(OF_BLENDMODE_DISABLED);
	modes.push_back(OF_BLENDMODE_ALPHA);
	modes.push_back(OF_BLENDMODE_ADD);
	modes.push_back(OF_BLENDMODE_SUBTRACT);
	modes.push_back(OF_BLENDMODE_MULTIPLY);
	modes.push_back(OF_BLENDMODE_SCREEN);

	feedbackEffect.load("feedback");
	mixer.load("mixer");
}

void ofApp::addEffect(int &id) {
	string name =	allEffects[id];
	ofEffect effect;
	effect.load("mixer.vert", name + ".frag");
	effect.width = width;
	effect.height = height;
	effect.name = name;
	effect.gui = activeLayer->effectsList->add<ofxGuiGroup>(name);
	if(name == "br-co") {
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.floats[0]->set("brightness",0,-1,1));
		effect.gui->add(effect.floats[1]->set("contrast",1,-2,2));
		effect.gui->add(effect.vec2s[0]->set("res",ofVec2f(0,0),ofVec2f(0,0), ofVec2f(width, height)));
	} else if (name == "noise") {
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.ints.push_back(new ofParameter<int>());
		effect.ints.push_back(new ofParameter<int>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.floats[0]->set("alpha",0,0,1));
		effect.gui->add(effect.floats[1]->set("speed",0.01,0,0.1));
		effect.gui->add(effect.floats[2]->set("amount",1,0,5));
		effect.gui->add(effect.floats[3]->set("scale",1,0,500));
		effect.gui->add(effect.ints[0]->set("audioChannel",0,0,audioBins.size()-2));
		effect.gui->add(effect.floats[4]->set("audioMultiplier",0,0,20));
		effect.gui->add(effect.vec2s[0]->set("res",ofVec2f(width,height),ofVec2f(0,0), ofVec2f(width, height)));
		effect.gui->add(effect.ints[1]->set("mono",0,0,1));
	} else if (name == "hsv") {
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.gui->add(effect.floats[0]->set("hue",0,0,2));
		effect.gui->add(effect.floats[1]->set("saturation",1,0,5));
		effect.gui->add(effect.floats[2]->set("value",1,0,5));
	} else if (name == "dsf") {
		effect.ints.push_back(new ofParameter<int>());
		effect.floats.push_back(new ofParameter<float>());
		effect.vec3s.push_back(new ofParameter<ofVec3f>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.ints[0]->set("audioChannel",0,0,audioBins.size()-2));
		effect.gui->add(effect.floats[0]->set("audioMultiplier",0,0,20));
		effect.gui->add(effect.vec3s[0]->set("copies",ofVec3f(1,1,1),ofVec3f(1,1,1), ofVec3f(100, 100, 100)));
		effect.gui->add(effect.vec2s[0]->set("resolution",ofVec2f(width,height),ofVec2f(0,0), ofVec2f(width, height)));
	} else if (name == "shapes") {
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.floats[0]->set("speed",0,0,1));
		effect.gui->add(effect.floats[1]->set("speedY",0,0,0.1));
		effect.gui->add(effect.floats[2]->set("radialS",1.5,0,20));
		effect.gui->add(effect.floats[3]->set("radialT",0.00001,0,0.1));
		effect.gui->add(effect.floats[4]->set("timeB",0,0,20));
		effect.gui->add(effect.floats[5]->set("finalB",0,0,20));
		effect.gui->add(effect.vec2s[0]->set("center",ofVec2f(0.5,0.5),ofVec2f(-1,-1), ofVec2f(2, 2)));
		effect.gui->add(effect.vec2s[1]->set("rectS",ofVec2f(0.5,0.5),ofVec2f(0,0), ofVec2f(100, 100)));
		effect.gui->add(effect.vec2s[2]->set("resolution",ofVec2f(width,height),ofVec2f(0,0), ofVec2f(width, height)));
	} else if (name == "blur") {
		effect.ints.push_back(new ofParameter<int>());
		effect.ints.push_back(new ofParameter<int>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.floats.push_back(new ofParameter<float>());
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.ints[0]->set("audioChannel",0,0,audioBins.size()-2));
		effect.gui->add(effect.floats[0]->set("audioMultiplier",1,0,20));
		effect.gui->add(effect.ints[1]->set("samples",0,0,10));
		effect.gui->add(effect.floats[1]->set("blur_size",0,0,50));
		effect.gui->add(effect.floats[2]->set("alpha",1,0,1));
		effect.gui->add(effect.vec2s[0]->set("resolution",ofVec2f(width,height),ofVec2f(0,0), ofVec2f(width, height)));
	} else if (name == "glitch") {
		effect.vec2s.push_back(new ofParameter<ofVec2f>());
		effect.gui->add(effect.vec2s[0]->set("resolution",ofVec2f(1,1),ofVec2f(0,0), ofVec2f(2, 2)));
	}

	activeLayer->effects.push_back(effect);
	activeLayer->activeEffectId = activeLayer->effects.size() - 1;
	activeLayer->activeEffectGui = effect.gui;
	effectParameters[name].set(false);
}

void ofApp::removeEffect() {
	int id = activeLayer->activeEffectId;
	cout << id << endl;
	activeLayer->effectsList->removeChild(activeLayer->activeEffectGui);
	activeLayer->effects.erase(activeLayer->effects.begin() + id);
}

void ofApp::effectMoved(MovingElementData &data) {
	cout << data.widget->parent()->parent()->getId() << endl;
	ofVJObj *layer = layers[stoi(data.widget->parent()->parent()->getId())];
	ofEffect effect = layer->effects[data.old_index];
	layer->effects.erase(layer->effects.begin() + data.old_index);
	layer->effects.insert(layer->effects.begin() + data.new_index, effect);
	layer->activeEffectId = data.new_index;
}

//// Init layers
void ofApp::initLayers() {
	layerPanelGui = gui.addPanel("Files", containerConf);
	layerPanelGui->add(addFileToggle.set("Add File"));
	layerPanelGui->add(removeFileToggle.set("Remove File"));
	addFileToggle.addListener(this, &ofApp::addLayer);
	removeFileToggle.addListener(this, &ofApp::removeLayer);

	layersGui = layerPanelGui->add<ofxSortableList>("Layers");
	layersGui->setExclusiveToggles(true);
	layersGui->getActiveToggleIndex().addListener(this, &ofApp::selectLayer);
	ofAddListener(layersGui->elementMoved, this, &ofApp::layerMoved);
}

void ofApp::addLayer() {
	ofFileDialogResult file = openFile();
	if(file.bSuccess) {
		layers.push_back(new ofVJObj());
		ofVJObj *layer = layers[layers.size() - 1];
		layer->id = layers.size() - 1;
		layer->load(file.getPath());
		layer->setLoopState(OF_LOOP_NORMAL);
		layer->setVolume(0);
		layer->play();
		layer->update();
		layer->setPaused(true);
		layer->effectsPanel = gui.addPanel(file.getName(), containerConf);
		layer->effectsPanel->add(layer->removeEffect.set("Remove effect"));
		layer->effectsPanel->setId(to_string(layer->id));
		layer->controlPanel = layer->effectsPanel->add<ofxGuiGroup>("Control Panel");
		layer->controlPanel->add(layer->playing.set("Play", false));
		layer->controlPanel->add(layer->playbackSpeed.set("Playback speed", 0, 0, 10));
		layer->controlPanel->add(layer->opacity.set("Opacity", 1, 0, 1));
		layer->controlPanel->add(layer->blendMode.set("BlendMode", 0, 0, 5));
		layer->controlPanel->add(layer->type.set("Type", 0, 0, 3));
		layer->controlPanel->add(layer->isWebCam.set("WebCam", false));
		layer->controlPanel->add(layer->isCamera.set("IPCamera", false));
		layer->controlPanel->add<ofxGuiTextField>(layer->userName.set("User name", ""));
		layer->controlPanel->add<ofxGuiTextField>(layer->userPassword.set("User password", ""));
		layer->controlPanel->add<ofxGuiTextField>(layer->camIP.set("Camera IP", ""));
		layer->playing.addListener(this, &ofApp::togglePlay);
		layer->playbackSpeed.addListener(this, &ofApp::setPlaybackSpeed);
		layer->removeEffect.addListener(this, &ofApp::removeEffect);
		layer->isWebCam.addListener(this, &ofApp::initWebCam);
		layer->isCamera.addListener(this, &ofApp::initCamera);
		layer->textures.push_back(ofFbo());
		layer->textures[0].allocate(width, height, GL_RGBA16);
		layer->textures.push_back(ofFbo());
		layer->textures[1].allocate(width, height, GL_RGBA16);

		selectLayer(layer->id);
		layersGui->add(layer->name.set(file.getName(), false));
		layer->effectsList = layer->effectsPanel->add<ofxSortableList>("Effects List");
		ofAddListener(layer->effectsList->elementMoved, this, &ofApp::effectMoved);
	}
}

void ofApp::handleLayer(ofVJObj *layer) {
	switch(layer->type.get()) {
		case 0: 
			if(!layer->isPaused()){
				layer->update();
				if(layer->effects.size() > 0) {
					layer->activeTexture.begin();
						layer->draw(0,0,width,height);
					layer->activeTexture.end();
					for(size_t i=0; i < layer->effects.size(); i++) {
						layer->applyEffect(&layer->effects[i]);
					}
				} else {
					layer->draw(0, 0, width, height);
				}
			}
			break;
		case 1: 
			if(layer->effects.size() > 0) {
				for(size_t i=0; i < layer->effects.size(); i++) {
					layer->applyEffect(&layer->effects[i]);
				}
			} else {
				layer->draw(0, 0, width, height);
			}
			break;
		case 2:
			if(layer->camera->isConnected()) {
				layer->camera->update();
				if(layer->effects.size() > 0) {
					layer->activeTexture.begin();
						layer->camera->draw(0,0,width,height);
					layer->activeTexture.end();
					for(size_t i=0; i < layer->effects.size(); i++) {
						layer->applyEffect(&layer->effects[i]);
					}
				} else {
					layer->camera->draw(0, 0, width, height);
				}
			}
			break;
		case 3:
			if(webcam.isInitialized()) {
				webcam.update();
				if(layer->effects.size() > 0) {
					layer->activeTexture.begin();
						webcam.draw(0,0,width,height);
					layer->activeTexture.end();
					for(size_t i=0; i < layer->effects.size(); i++) {
						layer->applyEffect(&layer->effects[i]);
					}
				} else {
					layer->draw(0, 0, width, height);
				}
			}
	}
}

void ofApp::removeLayer() {
	activeLayer->effectsPanel->parent()->removeChild(activeLayer->effectsPanel);
	layersGui->removeChild(layersGui->getControl(activeLayer->id));
	layers.erase(layers.begin() + activeLayer->id);
}

void ofApp::selectLayer(int &index) {
	activeLayer = layers[index];
}

void ofApp::layerMoved(MovingElementData &data) {
	ofVJObj *layer = layers[data.old_index];
	layer->id = data.new_index;
	layer->effectsPanel->setId(to_string(data.new_index));
	layers.erase(layers.begin() + data.old_index);
	layers.insert(layers.begin() + data.new_index, layer);
}

void ofApp::togglePlay(bool &state) {
	activeLayer->setPaused(!state);
}

void ofApp::setPlaybackSpeed(float &speed) {
	activeLayer->setSpeed(speed);
}

void ofApp::initWebCam(bool &state) {
	if(state) {
		webcam.setDeviceID(0);
		webcam.setDesiredFrameRate(60);
		webcam.initGrabber(640, 480);
		activeLayer->type = 3;
	} else {
		webcam.close();
		activeLayer->type = 0;
	}
}

void ofApp::initCamera(bool &state) {
	if(state) {
		activeLayer->type = 2;
		activeLayer->camera = make_shared<ofx::Video::IPVideoGrabber>();
		activeLayer->camera->setUsername(
			activeLayer->userName.get()
		);
		activeLayer->camera->setPassword(
			activeLayer->userPassword.get()
		);
		activeLayer->camera->setURI(
			activeLayer->camIP.get()
		);
		activeLayer->camera->connect();
	} else {
		activeLayer->type = 0;
		activeLayer->camera->disconnect();
	}
}

ofFileDialogResult ofApp::openFile() {
	ofFileDialogResult openFileResult = ofSystemLoadDialog("Select file"); 
	return openFileResult;
}

void ofApp::update() {
	for(size_t i=0; i < layers.size(); i++) {
		ofVJObj *layer = layers[i];
		layer->audio = audioBins;
		handleLayer(layer);
		layer_0.begin();
			mixer.begin();
				ofEnableBlendMode(modes[layer->blendMode.get()]);
				mixer.setUniform1f("opacity", layer->opacity.get());
				layer->activeTexture.draw(0,0,width,height);
				ofDisableBlendMode();
			mixer.end();
		layer_0.end();
	}

	output.begin();
		feedbackEffect.begin();	
			feedbackEffect.setUniformTexture("tex0", layer_0.getTexture(), 1);
			feedbackEffect.setUniformTexture("tex1", output.getTexture(), 2);
			feedbackEffect.setUniform1f("feedback", feedback.get());
			feedbackEffect.setUniform1i("samples", blurSamples.get());
			feedbackEffect.setUniform1f("brightness", brightness.get());
			feedbackEffect.setUniform1f("contrast", contrast.get());
			feedbackEffect.setUniform1f("hue", hue.get());
			feedbackEffect.setUniform1f("sat", saturation.get());
			feedbackEffect.setUniform1f("noise_opacity", noiseOpacity.get());
			feedbackEffect.setUniform1f("noise_contrast", noiseContrast.get());
			feedbackEffect.setUniform1f("noise_speed", noiseSpeed.get());
			feedbackEffect.setUniform1f("noise_scale", noiseScale.get());
			feedbackEffect.setUniform1f("noise_scale_2", noiseScale2.get());
			feedbackEffect.setUniform1f("noise_distance", noiseDistance.get());
			feedbackEffect.setUniform1f("noise_rotation", noiseRotation.get());
			feedbackEffect.setUniform1f("time", ofGetElapsedTimef());
			feedbackEffect.setUniform1f("blur_size", blurSize.get());
			feedbackEffect.setUniform2f("size", width, height);
			feedbackEffect.setUniform1f("audio", audioBins[audioChannel.get()] * audioMultiplier);
			layer_0.draw(0, 0, width, height);
		feedbackEffect.end();	
	output.end();
}

void ofApp::draw() {
	for(int i = 0; i < fft->getBinSize(); i++) {
		ofDrawLine(i * 3, 0, i * 3, audioBins[i] * 200);
	}
}

void ofApp::drawOutput(ofEventArgs &args) {
	output.draw(0,0,1280, 720);
}

void ofApp::drawPreview(ofEventArgs &args) {
	output.draw(0,0,1280 * 0.5, 720 * 0.5);
}

void ofApp::exit() {
	ofRemoveListener(layersGui->elementMoved, this, &ofApp::layerMoved);
}
