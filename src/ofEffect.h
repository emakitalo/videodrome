#pragma once

#include "ofMain.h"
#include "ofxGuiExtended.h"
#include "ofxSortableList.h"

class ofEffect : public ofShader {
	public:
		string name;
		ofxGuiGroup *gui;
		vector<ofParameter<float>*> floats;
		vector<ofParameter<int>*> ints;
		vector<ofParameter<ofVec2f>*> vec2s;
		vector<ofParameter<ofVec3f>*> vec3s;
		float width;
		float height;

		ofEffect() {
		}
};
