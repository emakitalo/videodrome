#pragma once

#include "ofEffect.h"

class ofMixer : public ofEffect {
	public:
		ofParameter<float> brightness;
		ofParameter<float> contrast;
		ofParameter<float> opacity;
};
