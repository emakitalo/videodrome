#version 150

uniform float time;
uniform vec2 resolution;
uniform float speed;
uniform float speedY;
uniform vec2 center;
uniform vec2 rectS;
uniform float radialS;
uniform float radialT;
uniform float timeB;
uniform float finalB;
out vec4 color;

float a = resolution.x / resolution.y;

float rand(float n){return fract(sin(n) * 43758.5453123);}

float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233)))*43758.5453123);
}

void main( void ) {
	vec2 position = ( gl_FragCoord.xy / resolution.xy );
	position.x *= a;
	vec2 rp = vec2(position.x, position.y + time * speedY);
	float t = smoothstep(0.0, timeB, rand(floor(1000.0 + time * speed) + radialT * distance(position, vec2(center.x * a, center.y))) * radialS);
	float sx = rand(floor(rp.y + t)) + t * rectS.x;
	float ix = floor(rp.x * sx);
	float fx = fract(rp.x * sx);
	float sy = rand(floor(rp.x + t)) + t * rectS.y;
	float iy = floor(rp.y * sy);
	float fy = fract(rp.y * sy);
	
	float r = mix(rand(iy), rand(ix + 1.0), fy);
	float g = mix(rand(iy), rand(ix + 1.0), fx);
	float b = mix(r * rand(ix), g * rand(ix + 1.0), smoothstep(0.0, finalB, g - r));

	color = vec4(r,g,b,1.0);
}
