#version 150

struct Material {
	vec3 diffuse;
	vec3 ambient;
	vec3 specular;
	float shininess;
};

Material mat = Material(
		vec3(1.0, 1.0, 1.0),
		vec3(0.0, 0.0, 0.0),
		vec3(1.0, 1.0, 1.0),
		5.0
		);

uniform float audio[16];
uniform float time;
uniform float audioMultiplier;
uniform int audioChannel;
uniform vec3 copies;
uniform vec2 resolution;
out vec4 color;

float snd = 0.0;
float speed = 0.0;
const int MAX_DEPTH = 100;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

vec3 rd = vec3(0.0, 0.0, 1.0);

vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
float permute(float x){return floor(mod(((x*34.0)+1.0)*x, 289.0));}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}
float taylorInvSqrt(float r){return 1.79284291400159 - 0.85373472095314 * r;}

vec4 grad4(float j, vec4 ip){
	const vec4 ones = vec4(1.0, 1.0, 1.0, -1.0);
	vec4 p,s;

	p.xyz = floor( fract (vec3(j) * ip.xyz) * 7.0 ) * ip.z - 1.0;
	p.w = 1.5 - dot(abs(p.xyz), ones.xyz);
	s = vec4(lessThan(p, vec4(0.0)));
	p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www; 

	return p;

}

float snoise(vec4 v){
	const vec2  C = vec2( 0.138196601125010504,  // (5 - sqrt(5))/20  G4
			0.309016994374947451); // (sqrt(5) - 1)/4   F4

	vec4 i  = floor(v + dot(v, C.yyyy) );
	vec4 x0 = v -   i + dot(i, C.xxxx);

	vec4 i0;

	vec3 isX = step( x0.yzw, x0.xxx  );
	vec3 isYZ = step( x0.zww, x0.yyz  );

	i0.x = isX.x + isX.y + isX.z;
	i0.yzw = 1.0 - isX;


	i0.y += isYZ.x + isYZ.y;
	i0.zw += 1.0 - isYZ.xy;

	i0.z += isYZ.z;
	i0.w += 1.0 - isYZ.z;

	vec4 i3 = clamp( i0, 0.0, 1.0  );
	vec4 i2 = clamp( i0-1.0, 0.0, 1.0  );
	vec4 i1 = clamp( i0-2.0, 0.0, 1.0  );

	vec4 x1 = x0 - i1 + 1.0 * C.xxxx;
	vec4 x2 = x0 - i2 + 2.0 * C.xxxx;
	vec4 x3 = x0 - i3 + 3.0 * C.xxxx;
	vec4 x4 = x0 - 1.0 + 4.0 * C.xxxx;

	i = mod(i, 289.0); 
	float j0 = permute( permute( permute( permute(i.w) + i.z ) + i.y ) + i.x );
	vec4 j1 = permute( permute( permute( permute (
						i.w + vec4(i1.w, i2.w, i3.w, 1.0 )
						)
					+ i.z + vec4(i1.z, i2.z, i3.z, 1.0 ))
				+ i.y + vec4(i1.y, i2.y, i3.y, 1.0 ))
			+ i.x + vec4(i1.x, i2.x, i3.x, 1.0 ));

	vec4 ip = vec4(1.0/294.0, 1.0/49.0, 1.0/7.0, 0.0) ;

	vec4 p0 = grad4(j0,   ip);
	vec4 p1 = grad4(j1.x, ip);
	vec4 p2 = grad4(j1.y, ip);
	vec4 p3 = grad4(j1.z, ip);
	vec4 p4 = grad4(j1.w, ip);

	vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
	p0 *= norm.x;
	p1 *= norm.y;
	p2 *= norm.z;
	p3 *= norm.w;
	p4 *= taylorInvSqrt(dot(p4,p4));

	vec3 m0 = max(0.6 - vec3(dot(x0,x0), dot(x1,x1), dot(x2,x2)), 0.0);
	vec2 m1 = max(0.6 - vec2(dot(x3,x3), dot(x4,x4)            ), 0.0);
	m0 = m0 * m0;
	m1 = m1 * m1;
	return 49.0 * ( dot(m0*m0, vec3( dot( p0, x0  ), dot( p1, x1  ), dot( p2, x2  ) ))
			+ dot(m1*m1, vec2( dot( p3, x3  ), dot( p4, x4  )  ) ) ) ;


}

float sminCubic( float a, float b, float k) {
	float h = max( k-abs(a-b), 0.0 )/k;
	return min( a, b ) - h*h*h*k*(1.0/6.0);
}

mat4 rotationMatrix(vec3 axis, float angle) {
	axis = normalize(axis);
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;

	return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
			oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
			oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
			0.0,                                0.0,                                0.0,                                1.0);
}

mat4 viewMatrix(vec3 eye, vec3 center, vec3 up) {
	vec3 f = normalize(center - eye);
	vec3 s = normalize(cross(f, up));
	vec3 u = cross(s, f);
	return mat4(
			vec4(s, 0.0),
			vec4(u, 0.0),
			vec4(-f, 0.0),
			vec4(0.0, 0.0, 0.0, 1)
			);
}

float sdBox( vec3 p, vec3 b) {
	vec3 d = abs(p) - b;
	return length(max(d,0.0))
		+ min(max(d.x,max(d.y,d.z)),0.0); // remove this line for an only partially signed sdf 
}

float sphereSDF(vec3 samplePoint) {
	return length(samplePoint) - 0.1;
}

float sdTorus( vec3 p, vec2 t) {
	vec2 q = vec2(length(p.xz)-t.x,p.y);
	return length(q)-t.y;
}

float intersectSDF(float distA, float distB) {
	return max(distA, distB);
}

float unionSDF(float distA, float distB) {
	return min(distA, distB);
}

float differenceSDF(float distA, float distB) {
	return max(distA, -distB);
}

float sdf(vec3 pt) {
	mat4 rot = rotationMatrix(vec3(snd * 0.01,1.0,-snd*0.01), -speed * 0.1);
	float box = sdBox((rot * vec4(pt, 1.0)).xyz, vec3(0.1, 0.1, 0.1) * snd);
	float sphere = sphereSDF(pt);
	float noise = snoise(vec4(pt, speed * 0.1 + snd * 0.01));
	return sminCubic(
		sphere,
		intersectSDF(
			noise,
			box
		)
		, 0.2 + snd * 0.01
	);
}

vec3 estimateNormal(vec3 p) {
	return normalize(vec3(
				sdf(vec3(p.x + EPSILON, p.y, p.z)) - sdf(vec3(p.x - EPSILON, p.y, p.z)),
				sdf(vec3(p.x, p.y + EPSILON, p.z)) - sdf(vec3(p.x, p.y - EPSILON, p.z)),
				sdf(vec3(p.x, p.y, p.z  + EPSILON)) - sdf(vec3(p.x, p.y, p.z - EPSILON))
				));
}

vec3 phongContribForLight(vec3 k_d, vec3 k_s, float alpha, vec3 p, vec3 eye,
	vec3 lightPos, vec3 lightIntensity) {
	vec3 N = estimateNormal(p);
	vec3 L = normalize(lightPos - p);
	vec3 V = normalize(eye - p);
	vec3 R = normalize(reflect(-L, N));

	float dotLN = dot(L, N);
	float dotRV = dot(R, V);

	if (dotLN < 0.0) {
		// Light not visible from this point on the surface
		return vec3(0.0, 0.0, 0.0);
	} 

	if (dotRV < 0.0) {
		// Light reflection in opposite direction as viewer, apply only diffuse
		// component
		return lightIntensity * (k_d * dotLN);
	}
	return lightIntensity * (k_d * dotLN + k_s * pow(dotRV, alpha));
}

vec3 phongIllumination(Material mat, vec3 p, vec3 eye) {
	const vec3 ambientLight = 0.5 * vec3(1.0, 1.0, 1.0);
	vec3 color = ambientLight * mat.ambient;

	vec3 light1Pos = vec3(4.0 * sin(speed),
			2.0,
			4.0 * cos(speed));
	vec3 light1Intensity = vec3(0.4, 0.4, 0.4);

	color += phongContribForLight(mat.diffuse, mat.specular, mat.shininess, p, eye,
			light1Pos,
			light1Intensity);

	vec3 light2Pos = vec3(2.0 * sin(0.37 * speed),
			2.0 * cos(0.37 * speed),
			2.0);
	vec3 light2Intensity = vec3(0.4, 0.4, 0.4) * (1.0 + snd * 0.1);

	color += phongContribForLight(mat.diffuse, mat.specular, mat.shininess, p, eye,
			light2Pos,
			light2Intensity);    
	return color;
}

float rayMarch(vec3 eye, vec3 rd, vec3 c, float start, float end) {
	float depth = start;
	for(int i=0; i < MAX_DEPTH; i++) {
		float dist = sdf(mod(eye + depth * rd, c) - 0.5 * c);
		if(dist < EPSILON) {
			return depth;
		}

		depth += dist;

		if(depth >= end) {
			return end;
		}

	}

	return end;
}

vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord) {
	vec2 xy = fragCoord - size / 2.0;
	float z = size.y / tan(radians(fieldOfView) / 2.0);
	return normalize(vec3(xy, -z));
}

void main( void ) {
	speed = time * 0.001;
	vec2 pos = gl_FragCoord.xy / resolution;
	vec3 eye = vec3(1.0, 1.0, snd);
	vec3 rd = rayDirection(45.0, resolution, gl_FragCoord.xy);
	snd = audio[audioChannel] * audioMultiplier;

	mat4 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));    
	vec3 worldDir = (viewToWorld * vec4(rd, 0.0)).xyz;

	float depth = rayMarch(
		eye,
		worldDir,
		copies,
		MIN_DIST,
		MAX_DIST
	);

	vec3 p = mod(eye + depth * worldDir, copies) - 0.5 * copies;

	vec3 col = phongIllumination(mat, p, eye);

	if(depth > MAX_DIST - EPSILON) {
		col *= 0.0;
	}

	color = vec4(col, 1.0);
}
