#version 150

uniform sampler2DRect tex0;
uniform float brightness;
uniform float contrast;
uniform vec2 res;
in vec2 t_coord;
out vec4 color;

void main()
{
  color = vec4((texture2DRect(tex0, t_coord).rgb * contrast) + brightness, 1.0);
}
