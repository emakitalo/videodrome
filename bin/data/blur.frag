#version 150

uniform sampler2DRect tex0;
uniform int samples;
uniform float blur_size;
uniform float alpha;
float snd = 0.0;
uniform float audio[16];
uniform float audioMultiplier;
uniform int audioChannel;
const float PI = 3.14159265359 * 0.5;
const float TRI = 3.14159265359 * 2.0 / 3.0;
in vec2 t_coord;
out vec4 color;

vec2 rotate_vec2(float angle){
  float x = cos(angle);
  float y = sin(angle);
  return vec2(0.0 * x - 1.0 * y, 0.0 * y + 1.0 * x);
}

void main() {
  vec4 layer_0 = texture2DRect(tex0, t_coord);
	snd = audio[audioChannel] * audioMultiplier;
	if(samples > 0) {
		for(float i=0; i < samples; i++){
			float rot = TRI + PI;
			float off = pow(i, 1.5) * snd;
			layer_0 += texture2DRect(tex0, t_coord + rotate_vec2(rot * i) * blur_size * off);
			layer_0 += texture2DRect(tex0, t_coord + rotate_vec2(rot * (i + 2.0)) * blur_size * off);
			layer_0 += texture2DRect(tex0, t_coord + rotate_vec2(rot * (i + 3.0)) * blur_size * off);
		}
		layer_0 /= 1.0 + (samples * 3.0);
	}
	color = vec4(layer_0.rgb, alpha);
}
