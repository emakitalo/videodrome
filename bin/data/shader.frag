#version 150

uniform float opacity;
uniform float brightness;
uniform float contrast;
uniform float feedback;
uniform float hue;
uniform int samples;
uniform int depth;
uniform float radius;
uniform float aspect;
uniform float time;
uniform vec2 res;
uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
in vec2 t_coord;
out vec4 color;
vec4 c;
vec4 o;
vec3 hsv;

vec3 rgb2hsv(vec3 c)
{
  vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
  vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
  vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

  float d = q.x - min(q.w, q.y);
  float e = 1.0e-10;
  return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 blur(int _samples, vec2 _uv, sampler2DRect _tex, float _radius)
{
  vec3 _color = texture2DRect(_tex, _uv).rgb;
  for(int i=0; i < _samples; i++)
  {
    if(i%2 == 0)
    {
      _color += texture2DRect(_tex, _uv + (vec2(1,1) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(-1,1) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(1,-1) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(-1,-1) * (_samples + 1) * _radius)).rgb;
    }
    else
    {
      _color += texture2DRect(_tex, _uv + (vec2(0,1) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(-1,0) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(1,0) * (_samples + 1) * _radius)).rgb;
      _color += texture2DRect(_tex, _uv + (vec2(0,-1) * (_samples + 1) * _radius)).rgb;
    }
  }

  _color = _color / ((_samples * 4) + 1);
  return _color;
}

// Simplex 2D noise
//
vec3 permute(vec3 x) { return mod(((x*34.0)+1.0)*x, 289.0); }

float snoise(vec2 v){
  const vec4 C = vec4(0.211324865405187, 0.366025403784439,
      -0.577350269189626, 0.024390243902439);
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);
  vec2 i1;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
  i = mod(i, 289.0);
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
      + i.x + vec3(0.0, i1.x, 1.0 ));
  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy),
        dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;
  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

float range(float low1, float high1, float low2, float high2, float value){
  return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
}

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
  //vec3 layer_1 = blur(depth, t_coord, tex1, radius) * (1.0 - feedback);

/*
  hsv = rgb2hsv(layer_1.rgb);

  float rand_1 = range(0.0, 1.0, -1.0, 1.0, rand(vec2(hsv.g, hsv.b))) * 0.1;
  float rand_2 = range(0.0, 1.0, -1.0, 1.0, rand(vec2(hsv.r, hsv.g))) * 0.1;


  hsv.r = mod(hue + layer_0.r, 1.0);
  hsv.g = mod(layer_0.r * layer_0.g * layer_0.b, 1.0);
  hsv.b *= 2.5;

  vec3 layer_0 = texture2DRect(tex0, t_coord + 
                               vec2(range(0.0, 1.0, -1.0, 1.0, snoise(vec2(hsv.g * time, hsv.b * time))), 
                                    range(0.0, 1.0, -1.0, 1.0, snoise(vec2(hsv.b * time, hsv.g * time)))) * 0.2).rgb  * opacity;

  o = vec4(layer_0 + layer_1, opacity);
*/

  color = vec4((layer_1 * contrast) + brightness, opacity);
}
